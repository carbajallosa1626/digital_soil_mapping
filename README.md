## Introducción

Como parte de la metodología de un estudio de suelos destinados a realizar un mapeo digital de suelos, es necesario contar con información sobre nuestro ámbito de estudio, para nuestro caso la Cuenca Chancay-Huaral. Por lo tanto, en esta oportunidad presentamos los procedimientos seguidos para obtener parte diversas fuente de datos, como por ejemplo las variables topográficas, logrado gracias al uso de herramientas como el ***Google Earth Engine*** (GEE) y el software R, empleandos paquetes específicos como ***rgee***, que nos permite unir ambas herramientas.

![](img/flow1.svg)